@extends('layouts.app')

@section('content')

   <div>   
             <img src="https://laravelnews.imgix.net/images/laravel-featured.png" class="img-fluid" alt="...">
   </div>

        <h1 class="text-center"> Featured Posts:</h1>

     @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="card text-center mt-3">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                </div>
            </div>
        @endforeach
    @else
        <div>
            <h2>There are no posts to show</h2>
        </div>
    @endif


@endsection

